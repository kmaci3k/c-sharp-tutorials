﻿using HanoiTower.exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HanoiTower
{
    class Game : IPrintable
    {
        private readonly List<Tower> towers = new List<Tower>();
        private uint height;
        private uint minimalMoves;
        private uint moves = 0;

        public void Start()
        {
            Console.Write("Enter height of Hanoi Tower: ");
            uint.TryParse(Console.ReadLine(), out height);
            minimalMoves = (uint) Math.Pow(2, height) - 1;

            Tower firstTower = PopulateTowers(height);
            PopulateDisks(firstTower, height);
        }

        private Tower PopulateTowers(uint height)
        {
            for(uint i = 0; i < 3; ++i)
            {
                towers.Add(new Tower(height));
            }
            return towers.First();
        }

        private void PopulateDisks(Tower tower, uint height)
        {
            for(uint diskSize = height; diskSize > 0; --diskSize)
            {
                tower.Push(new Disk(diskSize, height));
            }
        }

        public void Play()
        {

            PushDisk(PopDisk());
        }

        private Disk PopDisk()
        {
            Disk disk = null;
            do
            {
                int towerIdx = ChooseTower("Choose source tower (1-3): ");
                try
                {
                    disk = towers[towerIdx].Pop();
                }
                catch (EmptyTowerException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            } while (disk == null);
            return disk;
        }

        private void PushDisk(Disk disk)
        {
            bool moved = false;
            do
            {
                int towerIdx = ChooseTower("Choose target tower (1-3): ");
                try
                {
                    towers[towerIdx].Push(disk);
                    moved = true;
                }
                catch (LargerOnSmallerElementException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            } while (!moved);

            ++moves;
        }

        private int ChooseTower(string prompt)
        {
            int towerIdx;
            do
            {
                Console.Write(prompt);
                int.TryParse(Console.ReadLine(), out towerIdx);
            } while (towerIdx < 1 || towerIdx > 3);
            return towerIdx - 1;
        }

        public Boolean GameOver()
        {
            return (towers.Count > 0) && towers.Last().IsFull();
        }

        public void Print()
        {
            Console.Clear();
            Console.WriteLine("HanoiTower height: {0}", height);
            Console.WriteLine("Minimal moves: {0}", minimalMoves);
            Console.WriteLine("Moves: {0}", moves);
            Console.WriteLine();

            int cursorStartY = Console.CursorTop;
            for(int i = 0; i < towers.Count; ++i)
            {
                int cursorStartX = i * (2 * (int)(height + 1) + 2);
                Console.SetCursorPosition(cursorStartX, cursorStartY);

                towers[i].Print();
            }
            Console.Write("\n\n");
        }
    }
}
