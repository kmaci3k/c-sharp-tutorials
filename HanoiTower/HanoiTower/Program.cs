﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HanoiTower
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();

            game.Start();
            while(!game.GameOver())
            {
                game.Print();
                game.Play();
            }

            game.Print();

            Console.WriteLine("\nGAME OVER !!!");
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey();
        }
    }
}
