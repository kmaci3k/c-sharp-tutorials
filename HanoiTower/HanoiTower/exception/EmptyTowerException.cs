﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HanoiTower.exception
{
    class EmptyTowerException : Exception
    {
        public EmptyTowerException()
            : base ("Tower is empty")
        {

        }
    }
}
