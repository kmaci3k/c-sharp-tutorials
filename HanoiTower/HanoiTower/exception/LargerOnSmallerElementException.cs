﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HanoiTower.exception
{
    class LargerOnSmallerElementException : Exception
    {
        public LargerOnSmallerElementException()
            : base("Larger element was placed on smaller element")
        {

        }
    }
}
