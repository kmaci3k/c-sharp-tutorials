﻿using HanoiTower.exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HanoiTower
{
    class Tower : IPrintable
    {
        private readonly List<Disk> disks = new List<Disk>();
        public uint Height { get; private set; }

        public Tower(uint height)
        {
            this.Height = height;
        }

        public IList<Disk> Disks
        {
            get
            {
                return disks.AsReadOnly();
            }
        }

        public void Push(Disk disk)
        {
            if(disks.Count > 0)
            {
                Disk last = disks.Last();
                if(disk.CompareTo(last) > 0)
                {
                    throw new LargerOnSmallerElementException();
                }
            }
            disks.Add(disk);
        }

        public Disk Pop()
        {
            if (disks.Count == 0)
            {
                throw new EmptyTowerException();
            }
            Disk disk = disks.Last();
            disks.Remove(disk);

            return disk;
        }

        public Boolean IsFull()
        {
            return disks.Count == Height;
        }

        public void Print()
        {
            PrintEmptySpace();

            int cursorStartY = Console.CursorTop;
            int cursorStartX = Console.CursorLeft;
            for (int i = disks.Count - 1; i >= 0; --i)
            {
                disks[i].Print();
                Console.SetCursorPosition(cursorStartX, ++cursorStartY);
            }
            PrintBase();
        }

        private void PrintEmptySpace()
        {
            int cursorStartY = Console.CursorTop;
            int cursorStartX = Console.CursorLeft;
            for (uint i = 0; i < Height - disks.Count + 1; ++i)
            {
                Disk.PrintEmptyDisk(Height);
                Console.SetCursorPosition(cursorStartX, ++cursorStartY);
            }
        }

        private void PrintBase()
        {
            for(uint i = 0; i < 2 * (Height + 1) + 1; ++i)
            {
                Console.Write("-");
            }
        }
    }
}
