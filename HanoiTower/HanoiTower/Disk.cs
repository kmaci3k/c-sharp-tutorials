﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HanoiTower
{
    class Disk : IComparable<Disk>, IPrintable
    {
        public uint Size { get; private set; }
        public uint TowerHeight { get; private set; }
        public Disk(uint size, uint towerHeight)
        {
            this.Size = size;
            this.TowerHeight = towerHeight;
        }

        public int CompareTo(Disk other)
        {
            if(Size > other.Size)
            {
                return 1;
            }
            else if(Size < other.Size)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }

        public void Print()
        {
            PrintOffset();
            PrintSide();
            PrintSeparator();
            PrintSide();
        }

        public static void PrintEmptyDisk(uint height)
        {
            for (uint i = 0; i < height + 1; ++i)
            {
                Console.Write(" ");
            }
            Console.Write("|");
        }

        private void PrintOffset()
        {
            for (uint i = 0; i < TowerHeight - Size + 1; ++i)
            {
                Console.Write(" ");
            }
        }

        private void PrintSide()
        {
            for (uint i = 0; i < Size; ++i)
            {
                Console.Write("*");
            }
        }

        private void PrintSeparator()
        {
            Console.Write("|");
        }
    }
}
