﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarSimulator
{
    class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.Simulator();

            Console.WriteLine("Press any key to continue...");
            Console.ReadLine();
        }

        public void Simulator()
        {
            Console.WriteLine("Enter mark of car:");
            String mark = Console.ReadLine();
            Console.WriteLine("Enter model of car:");
            String model = Console.ReadLine();
            Console.WriteLine("Enter engine capacity:");
            float capacity = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter starting amout of fuel:");
            float amountOfFuel = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter distance to go [km]:");
            int distance = int.Parse(Console.ReadLine());

            Engine engine = new Engine(capacity, amountOfFuel);
            Car myCar = new Car(mark, model, engine);

            bool continueDrive;
            do
            {
                distance = myCar.Go(distance);

                Console.WriteLine("Do you want to continue drive ? [y/n]");
                continueDrive = Console.ReadLine().Equals("y");
                if(continueDrive)
                {
                    Console.WriteLine("Enter distance to go [km]:");
                    distance += int.Parse(Console.ReadLine());

                    Console.WriteLine("Do you want to refuel ? [y/n]");
                    bool refuel = Console.ReadLine().Equals("y");
                    if(refuel)
                    {
                        Console.WriteLine("How much (max: {0:F}) ?", engine.TankCapacity - engine.AmountOfFuel);
                        float amountOfRefuel = float.Parse(Console.ReadLine());
                        amountOfRefuel = Math.Min(amountOfRefuel, engine.TankCapacity - engine.AmountOfFuel);
                        engine.Refuel(amountOfRefuel);
                    }
                }

            } while (continueDrive);
        }

        public void MyCarTest()
        {
            Engine engine = new Engine(1.2f, 10f);
            Car myCar = new Car("KIA", "RIO III", engine);
            myCar.Go(10);
        }

        public void ConverterTest()
        {
            Engine engine = new Engine(1.2f, 10f);
            Console.WriteLine("{0:F} liters per 100km = {1:F} miles per gallon", 1.0f, engine.LitersPerHundredKmToMilesPerGallon(1.0f));
            Console.WriteLine("{0:F} miles per gallon = {1:F} liters per 100km", 35f, engine.MilesPerGallonToLitersPerHundredKm(35f));
        }
    }
}
