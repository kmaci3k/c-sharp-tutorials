﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CarSimulator
{
    class Car
    {
        private String mark;
        private String model;
        private readonly CarAnimation animation = new CarAnimation();
        public Engine Engine
        {
            get; private set;
        }

        public Car(String mark, String model, Engine engine)
        {
            this.mark = mark;
            this.model = model;
            this.Engine = engine;
        }

        public Car(String mark, String model, float capacity, float amountOfFuel)
            : this(mark, model, new Engine(capacity, amountOfFuel))
        {

        }

        public Car(String mark, String model, float capacity, float amountOfFuel, float tankCapacity)
            : this(mark, model, new Engine(capacity, amountOfFuel, tankCapacity))
        {

        }

        public int Go(int distance)
        {
            while (distance > 0)
            {
                Console.Clear();
                Console.SetCursorPosition(0, 0);

                bool isWorking = Engine.Work();
                if (isWorking)
                {
                    --distance;
                    Console.WriteLine("Distance to go: " + distance);
                    Console.WriteLine(mark + " " + model + " is going");
                }
                else
                {
                    Console.WriteLine("Distance to go: " + distance);
                    Console.WriteLine("Car stopped (no fuel)");
                    break;
                }
                animation.animate();
                Thread.Sleep(100);
            }
            return distance;
        }
    }
}
