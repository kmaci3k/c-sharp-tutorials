﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarSimulator
{
    class Engine
    {
        private const float DEFAULT_TANK_CAPACITY = 43;

        private float capacity;
        public float AmountOfFuel { get; private set; }
        public float TankCapacity { get; private set; }

        public Engine(float capacity, float amountOfFuel)
        {
            this.capacity = capacity;
            this.AmountOfFuel = amountOfFuel;
            this.TankCapacity = DEFAULT_TANK_CAPACITY;
        }

        public Engine(float capacity, float amountOfFuel, float tankCapacity)
            : this(capacity, amountOfFuel)
        {
            this.TankCapacity = tankCapacity;
        }

        public bool Work()
        {
            if(AmountOfFuel > 0)
            {
                AmountOfFuel -= 4.0f * capacity / ONE_HUNDRED_KM;
                Console.WriteLine("Engine is working !!! (fuel left: {0:F})", AmountOfFuel);
                return true;
            }
            else
            {
                Console.WriteLine("Run out of fuel !!!");
                return false;
            }
        }

        public void Refuel(float amountOfFuel)
        {
            this.AmountOfFuel += amountOfFuel;
        }

        public void Refuel()
        {
            Refuel(DEFAULT_TANK_CAPACITY);
        }

        private const float ONE_MILE_AS_KM = 1.6f;
        private const float ONE_GALLON_AS_LITERS = 3.78f;
        private const float ONE_HUNDRED_KM = 100;

        public float LitersPerHundredKmToMilesPerGallon(float combustionPerHunderKm)
        {
            float kilometersPerGallon = ONE_GALLON_AS_LITERS * ONE_HUNDRED_KM / combustionPerHunderKm;
            float milesPerGallon = kilometersPerGallon / ONE_MILE_AS_KM;
            return milesPerGallon;
        }

        public float MilesPerGallonToLitersPerHundredKm(float milesPerGallon)
        {
            float milesPerLiter = milesPerGallon / ONE_GALLON_AS_LITERS;
            float kilometersPerLiter = milesPerLiter * ONE_MILE_AS_KM;
            float litersPerHundredKm = ONE_HUNDRED_KM / kilometersPerLiter;
            return litersPerHundredKm;
        }
    }
}
