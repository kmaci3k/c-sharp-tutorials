﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarSimulator
{
    class CarAnimation
    {
        private int state = 0;
        private int maxState = 4;
        private char[] states = { '|', '/', '-', '\\' };

        public void animate()
        {
            Console.WriteLine(states[state]);
            state = ++state % maxState;
        }
    }
}
