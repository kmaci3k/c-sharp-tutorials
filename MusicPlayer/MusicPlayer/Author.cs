﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicPlayer
{
    class Author
    {
        internal String FirstName { get; private set; }
        internal String LastName { get; private set; }

        internal Author(String firstName, String lastName)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
        }

        public override string ToString()
        {
            return String.Format("Autor: {0} {1}", FirstName, LastName);
        }
    }
}
