﻿using MusicPlayer.Genres;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MusicPlayer
{
    class Player
    {
        private const int PLAY_TIMEOUT = 5000;
        public List<Genre> AvaiableGenres { get; private set; }
        private List<Song> songs = new List<Song>();

        internal Player()
        {
            AvaiableGenres = new List<Genre>
            {
                new Blues("Blues"),
                new Jazz("Jazz"),
                new Metal("Metal"),
                new Pop("POP"),
                new Rock("Rock")
            };
        }

        internal void PlayAll()
        {
            Console.WriteLine("\nOdtwarzanie wszystkich piosenek...");
            Console.WriteLine("----------------------------------");
            for(int i = 0; i < songs.Count; ++i)
            {
                Song song = songs.ElementAt(i);
                Console.WriteLine("\nUtwor {0}:\n--------\n{1}\n", i, song);
                song.Play();
                Console.WriteLine();

                Thread.Sleep(PLAY_TIMEOUT);
            }
            Console.WriteLine("-------------------------");
            Console.WriteLine("Lista utworow jest pusta.\n");
        }

        internal void AddSong(Song song)
        {
            songs.Add(song);
        }

        internal void RemoveSong(int number)
        {
            songs.RemoveAt(number);
        }

        internal void PrintAvailableGenres()
        {
            Console.WriteLine("\nLista dostepnych gatunkow muzyki:");
            Console.WriteLine("---------------------------------");
            for (int i = 0; i < AvaiableGenres.Count; ++i)
            {
                Console.WriteLine("{0}. {1}", i, AvaiableGenres.ElementAt(i).Name);
            }
        }
    }
}
