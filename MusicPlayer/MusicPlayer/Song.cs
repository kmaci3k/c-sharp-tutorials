﻿using MusicPlayer.Genres;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicPlayer
{
    class Song
    {
        private Author author;
        private String title;
        private Genre genre;
        internal Song(Author author, String title, Genre genre)
        {
            this.author = author;
            this.title = title;
            this.genre = genre;
        }
        internal void Play()
        {
            genre.Play();
        }

        public override string ToString()
        {
            return String.Format("{0}\nTytul: {1}\n{2}", author, title, genre);
        }
    }
}
