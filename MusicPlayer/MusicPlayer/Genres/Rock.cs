﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicPlayer.Genres
{
    class Rock : Genre
    {
        internal Rock(String name)
            : base(name)
        {

        }
        internal override void Play()
        {
            base.Play();
            Console.WriteLine("Sth specific for {0}", typeof(Rock));
        }
    }
}
