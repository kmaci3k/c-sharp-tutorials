﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicPlayer.Genres
{
    abstract class Genre
    {
        internal String Name { get; private set; }

        protected Genre(String name)
        {
            this.Name = name;
        }

        internal virtual void Play()
        {
            Console.WriteLine("Playing {0}", Name);
        }

        public override String ToString()
        {
            return String.Format("Gatunek: {0}", Name);
        }
    }
}
