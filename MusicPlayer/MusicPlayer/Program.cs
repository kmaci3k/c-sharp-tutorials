﻿using MusicPlayer.Genres;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicPlayer
{
    class Program
    {
        private Player player = new Player();

        static void Main(string[] args)
        {
            Program program = new Program();
            program.Run();

            Console.WriteLine("Nacisnij dowolny przycisk...");
            Console.ReadLine();
        }

        void Run()
        {
            String answer;
            do
            {
                Console.Write("Czy chcesz dodac piosenke do listy ? (y/n)\n> ");
                answer = Console.ReadLine();
                if (answer.Equals("y"))
                {
                    Author author = ReadAuthor();
                    String title = ReadTitle();
                    Genre genre = ChooseGenre();

                    Song song = new Song(author, title, genre);
                    player.AddSong(song);
                }
            } while (answer.Equals("y"));

            player.PlayAll();
        }

        Author ReadAuthor()
        {
            Console.WriteLine("Wykonawca: ");
            Console.WriteLine("-----------");
            Console.Write("Imie: ");
            String firstName = Console.ReadLine();
            Console.Write("Nazwisko: ");
            String lastName = Console.ReadLine();
            return new Author(firstName, lastName);
        }

        String ReadTitle()
        {
            Console.Write("Tytul:\n> ");
            return Console.ReadLine();
        }

        Genre ChooseGenre()
        {
            player.PrintAvailableGenres();
            int answer;
            do
            {
                Console.Write("Twoj wybor:\n> ");
                answer = int.Parse(Console.ReadLine());
            } while (answer >= player.AvaiableGenres.Count);

            return player.AvaiableGenres.ElementAt(answer);
        }
    }
}
