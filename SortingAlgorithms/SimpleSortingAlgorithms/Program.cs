﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithmsSpimple
{
    class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.TestSuite();

            Console.WriteLine("Press any key to exit...");
            Console.ReadLine();
        }

        private void Generate(int[] data)
        {
            Random rnd = new Random();
            int max = data.Length;
            for (int i = 0; i < data.Length; ++i)
            {
                data[i] = rnd.Next(0, max);
            }
        }

        public void TestSuite()
        {
            int multiply = 100;
            for (int i = 50 * multiply; i < 200 * multiply; i += 10 * multiply)
            {
                Console.WriteLine("<----- Data size: " + i + " ----->");
                int[] selectionData = new int[i];

                Generate(selectionData);

                Console.WriteLine("Selection sort");
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();

                Sort(selectionData);

                stopWatch.Stop();
                Console.WriteLine("Execution time: " + stopWatch.ElapsedMilliseconds + " ms");
            }
        }

        private int FindMinimum(int[] data, int startIndex)
        {
            int min = int.MaxValue;
            int minIndex = 0;
            for (int i = startIndex; i < data.Length; ++i)
            {
                if (data[i] < min)
                {
                    min = data[i];
                    minIndex = i;
                }
            }
            return minIndex;
        }

        private void Swap(int[] data, int from, int to)
        {
            int temp = data[from];
            data[from] = data[to];
            data[to] = temp;
        }

        private void Sort(int[] data)
        {
            for (int i = 0; i < data.Length - 1; ++i)
            {
                int minIndex = FindMinimum(data, i);
                Swap(data, i, minIndex);
            }
        }
    }
}
