﻿using SortingAlgorithms.generators;
using System;
using System.Diagnostics;

namespace SortingAlgorithms
{
    class Program
    {
        private SortingAlgorithm insertionSort = new InsertionSort();
        private SortingAlgorithm selectionSort = new SelectionSort();
        private SortingAlgorithm coctailSort = new CoctailSort();
        private SortingAlgorithm heapSort = new HeapSort();

        static void Main(string[] args)
        {
            Program program = new Program();

            Console.WriteLine("<----- Random generator ----->");
            program.TestSuite(new RandomGenerator());

            Console.WriteLine("Press any key to run next test...");
            Console.ReadLine();

            Console.WriteLine("<----- Increasing generator ----->");
            program.TestSuite(new IncreasingGenerator());

            Console.WriteLine("Press any key to run next test...");
            Console.ReadLine();

            Console.WriteLine("<----- Decreasing generator ----->");
            program.TestSuite(new DecreasingGenerator());

            Console.WriteLine("Press any key to run next test...");
            Console.ReadLine();

            Console.WriteLine("<----- Constant generator ----->");
            program.TestSuite(new ConstantGenerator());

            Console.WriteLine("Press any key to run next test...");
            Console.ReadLine();

            Console.WriteLine("<----- V-shape generator ----->");
            program.TestSuite(new VGenerator());

            Console.WriteLine("Press any key to exit...");
            Console.ReadLine();
        }

        public void TestSuite(Generator generator)
        {
            int multiply = 100;
            for (int i = 50 * multiply; i < 200 * multiply; i += 10 * multiply)
            {
                Console.WriteLine("<----- Data size: " + i + " ----->");
                int[] insertionData = new int[i];
                int[] selectionData = new int[i];
                int[] coctailData = new int[i];
                int[] heapData = new int[i];

                generator.Generate(insertionData);

                Array.Copy(insertionData, selectionData, i);
                Array.Copy(insertionData, coctailData, i);
                Array.Copy(insertionData, heapData, i);

                Console.WriteLine("Insertion sort");
                Test(insertionSort, insertionData);
                Console.WriteLine("Selection sort");
                Test(selectionSort, selectionData);
                Console.WriteLine("Coctail sort");
                Test(coctailSort, coctailData);
                Console.WriteLine("Heap sort");
                Test(heapSort, heapData);
            }
        }

        private void Test(SortingAlgorithm alg, int[] data)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            alg.Sort(data);

            stopWatch.Stop();
            Console.WriteLine("Execution time: " + stopWatch.ElapsedMilliseconds + " ms");
        }
    }
}
