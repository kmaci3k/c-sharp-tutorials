﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms
{
    public class CoctailSort : SortingAlgorithm
    {
        public override void Sort(int[] data)
        {
            int bottom = 0;
            int top = data.Length - 1;
            bool zamiana = true;
            while (zamiana)
            {
                zamiana = false;
                for (int i = bottom; i < top; i = i + 1)
                {
                    if (data[i] > data[i + 1]) 
                    {
                        Swap(data, i, i + 1);
                        zamiana = true;
                    }
                }

                --top;
                for (int i = top; i > bottom; i = i - 1)
                {
                    if (data[i] < data[i - 1])
                    {
                        Swap(data, i, i - 1);
                        zamiana = true;
                    }
                }

                ++bottom;
            }
        }
    }
}
