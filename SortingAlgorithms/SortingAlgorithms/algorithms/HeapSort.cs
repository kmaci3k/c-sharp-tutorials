﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms
{
    public class HeapSort : SortingAlgorithm
    {
        private void construct(int[] data)
        {
            for(int i = 1; i < data.Length; ++i)
            {
                for(int j = i, k = (j - 1) / 2; j > 0 && data[j] > data[k]; j = k, k = (j - 1) / 2)
                {
                    Swap(data, j, k);
                }
            }
        }

        public override void Sort(int[] data)
        {
            construct(data);
            for(int i = data.Length - 1; i > 0; --i)
            {
                Swap(data, 0, i);
                for (int k = 0; (2 * k + 1) < i;)
                {
                    int left = 2 * (k) + 1;
                    int right = 2 * (k + 1);
                    int greaterIndex;
                    if (right < i && data[right] > data[left])
                    {
                        greaterIndex = right;
                    }
                    else
                    {
                        greaterIndex = left;
                    }
                    if(data[k] < data[greaterIndex])
                    {
                        Swap(data, k, greaterIndex);
                        k = greaterIndex;
                    }
                    else
                    {
                        break;
                    }
                }

            }
        }
    }
}
