﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms
{
    public class SelectionSort : SortingAlgorithm
    {
        private int findMinimum(int[] data, int startIndex)
        {
            int min = int.MaxValue;
            int minIndex = 0;
            for(int i = startIndex; i < data.Length; ++i)
            {
                if(data[i] < min)
                {
                    min = data[i];
                    minIndex = i;
                }
            }
            return minIndex;
        }
        public override void Sort(int[] data)
        {
            for(int i = 0; i < data.Length - 1; ++i)
            {
                int minIndex = findMinimum(data, i);
                Swap(data, i, minIndex);
            }
        }
    }
}
