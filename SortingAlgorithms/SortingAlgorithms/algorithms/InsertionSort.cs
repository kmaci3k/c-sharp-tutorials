﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms
{
    class InsertionSort : SortingAlgorithm
    {
        public override void Sort(int[] data)
        {
            for(int i = 1; i < data.Length; ++i)
            {
                int current = data[i];
                for(int j = i; j > 0 && current < data[j - 1]; --j)
                {
                    Swap(data, j, j - 1);
                }
            }
        }
    }
}
