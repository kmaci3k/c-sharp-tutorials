﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms
{
    public abstract class SortingAlgorithm
    {
        public abstract void Sort(int[] data);
        protected void Swap(int[] data, int from, int to)
        {
            int temp = data[from];
            data[from] = data[to];
            data[to] = temp;
        }
        public void Print(int[] data)
        {
            foreach (int item in data)
            {
                Console.Write(item);
                Console.Write(" ");
            }
            Console.WriteLine();
        }
    }
}
