﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms.generators
{
    public class IncreasingGenerator : Generator
    {
        public void Generate(int[] data)
        {
            for (int i = 0; i < data.Length; ++i)
            {
                data[i] = i;
            }
        }
    }
}
