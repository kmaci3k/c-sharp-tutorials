﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms.generators
{
    public class RandomGenerator : Generator
    {
        private Random rnd = new Random();

        public void Generate(int[] data)
        {
            int max = data.Length;
            for (int i = 0; i < data.Length; ++i)
            {
                data[i] = rnd.Next(0, max);
            }
        }
    }
}
