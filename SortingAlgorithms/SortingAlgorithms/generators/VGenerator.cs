﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms.generators
{
    public class VGenerator : Generator
    {
        public void Generate(int[] data)
        {
            for (int i = 0; i < data.Length / 2; ++i)
            {
                data[i] = (data.Length / 2) - i;
            }

            for (int i = data.Length / 2; i < data.Length; ++i)
            {
                data[i] = i - (data.Length / 2);
            }
        }
    }
}
