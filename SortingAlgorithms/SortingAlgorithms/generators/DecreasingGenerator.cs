﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms.generators
{
    public class DecreasingGenerator : Generator
    {
        public void Generate(int[] data)
        {
            for (int i = data.Length - 1; i >= 0; --i)
            {
                data[i] = i;
            }
        }
    }
}
