﻿using DataAccess;
using DataAccess.exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.Test();
            //program.TestThanCanGetData();
            //program.TestThanThrowsExceptionWhenCredentialsAreInvalid();
            //program.TestThatThrowsExceptionWhenConnectionIsOpenedTwice();
            //program.TestThatThrowsExceptionWhenClosingNotOpenedConnection();
            //program.TestThatThrowsExceptionWhenTryingToGetDataThroughClosedConnection();

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        public void Test()
        {
            IDatabase db = new Database("Test database");
            Console.WriteLine("Program testowy\n-------------------\n");
            Console.WriteLine("Nazwa bazy danych: {0}\n", db.GetDatabaseName());

            int option = 0;
            IConnection connection = LogIn(db);
            do
            {
                Console.WriteLine("Menu\n-------------------");
                Console.WriteLine("0. Koniec");
                Console.WriteLine("1. Dane logowania do bazy danych");
                Console.WriteLine("2. Otworz polaczenie");
                Console.WriteLine("3. Pobierz dane");
                Console.WriteLine("4. Zamknij polaczenie");
                Console.Write(">  ");

                int.TryParse(Console.ReadLine(), out option);
                try {
                    if(option > 1)
                    {
                        executeOption(option, db, connection);
                        Console.WriteLine("\nOK\n");
                    }
                    else if(option == 1)
                    {
                        connection = openConnection(db);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                
            } while (option != 0);
        }

        private IConnection LogIn(IDatabase db)
        {
            Console.WriteLine("Inicjalizacja programu testowego\n-------------------");

            int option = 0;
            IConnection connection = null;
            do
            {
                Console.WriteLine("Menu\n-------------------");
                Console.WriteLine("1. Dane logowania do bazy danych");
                Console.Write(">  ");

                int.TryParse(Console.ReadLine(), out option);
                try
                {
                    if(option == 1)
                    {
                        connection = openConnection(db);
                        Console.WriteLine("\nOK\n");
                        return connection;
                    }
                    Console.WriteLine("Niepoprawna opcja !!!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            } while (connection == null);

            return connection;
        }

        private void executeOption(int option, IDatabase db, IConnection connection)
        {
            switch (option)
            {
                case 2:
                    {
                        connection.Open();
                        break;
                    }
                case 3:
                    {
                        printData(db.GetData(connection));
                        break;
                    }
                case 4:
                    {
                        connection.Close();
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Niepoprawna opcja !!!");
                        break;
                    }
            }
        }

        private IConnection openConnection(IDatabase db)
        {
            return db.GetNewConnection(readUsername(), readPassword());
        }

        private string readUsername()
        {
            Console.Write("Podaj nazwe uzytkownika: ");
            return Console.ReadLine();
        }

        private string readPassword()
        {
            Console.Write("Podaj haslo: ");
            return Console.ReadLine();
        }

        public void TestThanCanGetData()
        {
            Console.WriteLine("TestThanCanGetData");
            IDatabase db = new Database("Test database");
            using (IConnection connection = db.GetNewConnection("asdf", "123"))
            {
                connection.Open();
                printData(db.GetData(connection));
                connection.Close();
            }
            Console.WriteLine("---------------------------------------------------------\n");
        }

        private void printData(List<Dictionary<string, object>> data)
        {
            List<Person> people = data
                .Select(dictionary => Person.from(dictionary))
                .Where(person => (person.Pesel % 2) == 1)
                .OrderBy(person => person.Pesel)
                .ToList();
            people.ForEach(person => Console.WriteLine(person.ToString()));
        }

        public void TestThanThrowsExceptionWhenCredentialsAreInvalid()
        {
            Console.WriteLine("TestThanThrowsExceptionWhenCredentialsAreInvalid");
            IDatabase db = new Database("Test database");
            try
            {
                using (IConnection connection = db.GetNewConnection("invalid", "credentials"))
                {
                    connection.Open();
                    //the rest does not matter...
                }
            }
            catch (AuthenticationException ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("---------------------------------------------------------\n");
        }

        public void TestThatThrowsExceptionWhenConnectionIsOpenedTwice()
        {
            Console.WriteLine("TestThatThrowsExceptionWhenConnectionIsOpenedTwice");
            IDatabase db = new Database("Test database");
            try
            {
                using (IConnection connection = db.GetNewConnection("asdf", "123"))
                {
                    connection.Open();
                    connection.Open();
                    //the rest does not matter...
                }
            }
            catch (ConnectionAlreadyOpenedException ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("---------------------------------------------------------\n");
        }

        public void TestThatThrowsExceptionWhenClosingNotOpenedConnection()
        {
            Console.WriteLine("TestThatThrowsExceptionWhenOpeningClosedException");
            IDatabase db = new Database("Test database");
            try
            {
                using (IConnection connection = db.GetNewConnection("asdf", "123"))
                {
                    connection.Close();
                    //the rest does not matter...
                }
            }
            catch (ConnectionAlreadyClosedException ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("---------------------------------------------------------\n");
        }

        public void TestThatThrowsExceptionWhenTryingToGetDataThroughClosedConnection()
        {
            Console.WriteLine("TestThatThrowsExceptionWhenTryingToGetDataThroughClosedConnection");
            IDatabase db = new Database("Test database");
            try
            {
                using (IConnection connection = db.GetNewConnection("asdf", "123"))
                {
                    db.GetData(connection);
                    //the rest does not matter...
                }
            }
            catch (ConnectionClosedException ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("---------------------------------------------------------\n");
        }
    }
}
