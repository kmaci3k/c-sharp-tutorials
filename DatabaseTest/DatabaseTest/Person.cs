﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseTest
{
    class Person
    {
        private long pesel;
        private String firstName;
        private String lastName;

        Person(long pesel, String firstName, String lastName)
        {
            this.pesel = pesel;
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public long Pesel
        {
            get
            {
                return pesel;
            }
        }

        public String FirstName
        {
            get
            {
                return firstName;
            }
        }

        public String LastName
        {
            get
            {
                return lastName;
            }
        }

        public static Person from(Dictionary<string, object> values)
        {
            return new Person((long)values["pesel"], (string)values["imie"], (string)values["nazwisko"]);
        }

        public override string ToString()
        {
            return String.Format("PESEL: {0,11:00000000000}, Imie: {1}, Nazwisko: {2}", pesel, firstName, lastName);
        }
    }
}
