﻿using DataAccess.exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    internal class Connection : IConnection
    {
        private readonly IDatabase database;
        private readonly String username;
        private readonly String password;
        private bool isOpened = false;
        private bool isDisposed = false;

        public Connection(IDatabase database, String username, String password)
        {
            this.database = database;
            this.username = username;
            this.password = password;
        }

        public IDatabase Database
        {
            get
            {
                return database;
            }
        }

        public void Close()
        {
            if(!isOpened)
            {
                throw new ConnectionAlreadyClosedException();
            }
            isOpened = false;
        }

        public string GetPassword()
        {
            return password;
        }

        public string GetUsername()
        {
            return username;
        }

        public bool IsOpen()
        {
            return isOpened;
        }

        public void Open()
        {
            if(isOpened)
            {
                throw new ConnectionAlreadyOpenedException();
            }
            if(!username.Equals("asdf") || !password.Equals("123"))
            {
                throw new AuthenticationException();
            }
            isOpened = true;
        }

        #region IDisposable Support

        protected virtual void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                if (disposing)
                {
                    System.Console.WriteLine("Disposing Connection !!!");
                }

                isDisposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
