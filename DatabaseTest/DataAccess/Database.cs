﻿using DataAccess.exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class Database : IDatabase
    {
        private readonly String name;

        public Database(String name)
        {
            this.name = name;
        }

        public List<Dictionary<string, object>> GetData(IConnection connection)
        {
            if(!connection.IsOpen())
            {
                throw new ConnectionClosedException();
            }
            return populateData();
        }

        private List<Dictionary<string, object>> populateData()
        {
            List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
            Random rnd = new Random();
            for (int i = 9; i >= 0; --i)
            {
                Dictionary<string, object> item = new Dictionary<string, object>();
                long pesel = 0;
                for(long j = 1; j <= 10000000000; j *= 10)
                {
                    pesel += j * rnd.Next(0, 10);
                }
                item.Add("pesel", pesel);
                item.Add("imie", "imie_" + i);
                item.Add("nazwisko", "nazwisko_" + i);

                data.Add(item);
            }
            return data;
        }

        public string GetDatabaseName()
        {
            return name;
        }

        public IConnection GetNewConnection(string userName, string password)
        {
            return new Connection(this, userName, password);
        }
    }
}
