﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.exception
{
    public class ConnectionAlreadyClosedException : Exception
    {
        internal ConnectionAlreadyClosedException() : base("Provided connection is already closed")
        {

        }
    }
}
