﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.exception
{
    public class ConnectionAlreadyOpenedException : Exception
    {
        internal ConnectionAlreadyOpenedException() : base("Provided connection is already opened")
        {

        }
    }
}
