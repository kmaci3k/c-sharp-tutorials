﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.exception
{
    public class ConnectionClosedException : Exception
    {
        internal ConnectionClosedException() : base("Provided connection is closed")
        {
        }
    }
}
