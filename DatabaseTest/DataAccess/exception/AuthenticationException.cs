﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.exception
{
    public class AuthenticationException : Exception
    {
        internal AuthenticationException() : base("Invalid credentials")
        {

        }
    }
}
