﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using QuizModule.ViewModels;
using System.Windows.Controls.Primitives;
using System.Windows.Media.Animation;
using QuizModule.DataModel;
using System.Threading;
using System.ComponentModel;

namespace QuizModule.Views
{
    public partial class QuizView : UserControl
    {
        private readonly Storyboard wrongAnswerAnimation;
        private readonly Storyboard correctAnswerAnimation;
        private volatile Boolean animationInProgress = false;

        private delegate void AnswerHandler();
        private AnswerHandler onComplete;
        public static Grid AgGrid;
        public QuizView()
        {
            InitializeComponent();
            wrongAnswerAnimation = (Storyboard)TryFindResource("wrongAnswer");
            correctAnswerAnimation = (Storyboard)TryFindResource("correctAnswer");
            AgGrid = AgnieszkaGrid;
        }

        public QuizView(QuizViewModel viewModel)
            : this()
        {
            ViewModel = viewModel;
        }

        private QuizViewModel _viewModel;
        public QuizViewModel ViewModel
        {
            get { return _viewModel; }
            private set
            {
                if (_viewModel == null || !_viewModel.Equals(value))
                {
                    DataContext = value;
                    _viewModel = value;
                }
            }
        }

        private void UserControlLoaded(object sender, EventArgs e)
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;
            
            worker.RunWorkerAsync();
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                (sender as BackgroundWorker).ReportProgress(1);
                while (animationInProgress) ;
                Thread.Sleep(10);
              
            }
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (pbStatus.Value > 0)
            {
                pbStatus.Value = pbStatus.Value - 1;
            }
            else
            {
                ViewModel.Timeout();
                resetProgressBar();
            }
        }

        private void ViewSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (sender is QuizView)
            {
                QuizView view = (QuizView)sender;
                ItemsControl itemsControl = view.FindName("asdfg") as ItemsControl;

                ItemsPresenter itemsPresenter = GetVisualChild<ItemsPresenter>(itemsControl);
                UniformGrid itemsPanel = VisualTreeHelper.GetChild(itemsPresenter, 0) as UniformGrid;
                Size newSize = e.NewSize;
                if (newSize.Width < 400)
                {
                    itemsPanel.Columns = 1;
                }
                else if (newSize.Width >= 400 && newSize.Width < 600)
                {
                    itemsPanel.Columns = 2;
                }
                else
                {
                    itemsPanel.Columns = 4;
                }
            }
        }

        private static T GetVisualChild<T>(DependencyObject parent) where T : Visual
        {
            T child = default(T);

            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++)
            {
                Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);
                child = v as T;
                if (child == null)
                {
                    child = GetVisualChild<T>(v);
                }
                if (child != null)
                {
                    break;
                }
            }
            return child;
        }

        private void AnswerAMouseDown(object sender, MouseButtonEventArgs e)
        {
            HandleMouseDown(sender, ViewModel.CurrentQuestion.AnswerA);
        }

        private void AnswerBMouseDown(object sender, MouseButtonEventArgs e)
        {
            HandleMouseDown(sender, ViewModel.CurrentQuestion.AnswerB);
        }

        private void AnswerCMouseDown(object sender, MouseButtonEventArgs e)
        {
            HandleMouseDown(sender, ViewModel.CurrentQuestion.AnswerC);
        }

        private void AnswerDMouseDown(object sender, MouseButtonEventArgs e)
        {
            HandleMouseDown(sender, ViewModel.CurrentQuestion.AnswerD);
        }

        private void HandleMouseDown(object sender, Answer answer)
        {
            if (animationInProgress)
            {
                return;
            }

            Border border = sender as Border;
            if (answer.IsCorrect)
            {
                animationInProgress = true;
                border.BeginStoryboard(correctAnswerAnimation);
                onComplete = ViewModel.CorrectAnswer;
            }
            else
            {
                animationInProgress = true;
                border.BeginStoryboard(wrongAnswerAnimation);
            }
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            if (onComplete != null)
            {
                onComplete();
                resetProgressBar();
                onComplete = null;
            }
            animationInProgress = false;
        }

        private void resetProgressBar()
        {
            pbStatus.Value = 1000;
        }

        private void pbStatus_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ProgressBar p = (ProgressBar)sender;
            if (p.Template == null)
            {
                return;
            }

            //Rectangle progressBar = ((Grid)p.Template.FindName("Animation", p)).Children[0] as Rectangle;
            if (e.NewValue > 700)
            {
                //progressBar.Fill = Brushes.DarkBlue;
                p.Foreground = Brushes.DarkBlue;
            }
            else if (e.NewValue > 500)
            {
                // progressBar.Fill 
                p.Foreground = Brushes.Indigo;
            }
            else if (e.NewValue > 300)
            {
                //  progressBar.Fill =
                p.Foreground = Brushes.Purple;
            }
            else if (e.NewValue > 100)
            {
                //progressBar.Fill =
                p.Foreground = Brushes.Crimson;
            }
            else
            {
                // progressBar.Fill = 
                p.Foreground = Brushes.Red;
            }
        }
    }
}
