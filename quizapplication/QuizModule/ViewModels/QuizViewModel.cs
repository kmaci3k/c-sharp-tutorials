﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonModule.MVVM;
using QuizModule.Models;
using System.Windows.Input;
using System.Windows;
using Microsoft.Practices.Prism.Commands;
using System.Collections.ObjectModel;
using System.Windows.Media;
using QuizModule.DataModel;
using System.Xml.Linq;

namespace QuizModule.ViewModels
{
    public class QuizViewModel : ExtendedViewModel<QuizModel>
    {
        #region CurrentQuestion
        private QuizItem currentQuestion;
        
        public QuizItem CurrentQuestion
        {
            get { return currentQuestion; }
            set
            {
                if (currentQuestion == null || !currentQuestion.Equals(value))
                {
                    currentQuestion = value;
                    NotifyPropertyChanged("CurrentQuestion");
                }
            }
            
        }
        #endregion

        public static List<QuizItem> quizItems = new List<QuizItem>();

        public QuizViewModel() : base(new QuizModel())
        {
            
            List<QuizItem> testItems = new List<QuizItem>();
            testItems.Add(new QuizItem(new Question("Ile wynosi suma miar kątów wewnętrznych w trójkącie ?"),
                new Answer("90 st."),
                new Answer("180 st.", true),
                new Answer("270 st."), new Answer("360 st.")));

            testItems.Add(new QuizItem(new Question("Jaka waluta obowiązywała we Włoszech przed wprowadzeniem euro ?"),
                new Answer("Denar"),
                new Answer("Talar"),
                new Answer("Marka"), new Answer("Lir", true)));

            testItems.Add(new QuizItem(new Question("Ile zer ma miliard ?"),
                new Answer("6"),
                new Answer("8"),
                new Answer("9", true), new Answer("12")));

            saveToXml(testItems, "questions.xml");
            quizItems = loadFromXml("questions.xml");
            DrawQuestion();
        }

        public void saveToXml(List<QuizItem> data, String path)
        {
            XDocument doc = new XDocument();
            XElement root = new XElement("quizitems");
            doc.Add(root);

            foreach (QuizItem item in data)
            {
                XElement element = new XElement("quizitem");
                XElement question = new XElement("question", item.Question.Text);
                XElement answerA = new XElement("answerA", item.AnswerA.Text);
                XElement answerB = new XElement("answerB", item.AnswerB.Text);
                XElement answerC = new XElement("answerC", item.AnswerC.Text);
                XElement answerD = new XElement("answerD", item.AnswerD.Text);


                element.Add(question);
                element.Add(answerA);
                element.Add(new XElement("correct", item.AnswerA.IsCorrect));
                element.Add(answerB);
                element.Add(new XElement("correct", item.AnswerB.IsCorrect));
                element.Add(answerC);
                element.Add(new XElement("correct", item.AnswerC.IsCorrect));
                element.Add(answerD);
                element.Add(new XElement("correct", item.AnswerD.IsCorrect));

                root.Add(element);
            }

            doc.Save(path);
        }

        public List<QuizItem> loadFromXml(String path)
        {
            List<QuizItem> quizItems = new List<QuizItem>();

            XDocument doc = XDocument.Load(path);
            XElement root = doc.Elements().First();
            foreach (XElement element in root.Elements())
            {
                XElement q = element.Elements().ElementAt(0);
                Question question = new Question(q.Value);

                XElement a = element.Elements().ElementAt(1);
                Answer answerA = new Answer(a.Value, Boolean.Parse(element.Elements().ElementAt(2).Value));

                XElement b = element.Elements().ElementAt(3);
                Answer answerB = new Answer(b.Value, Boolean.Parse(element.Elements().ElementAt(4).Value));

                XElement c = element.Elements().ElementAt(5);
                Answer answerC = new Answer(c.Value, Boolean.Parse(element.Elements().ElementAt(6).Value));

                XElement d = element.Elements().ElementAt(7);
                Answer answerD = new Answer(d.Value, Boolean.Parse(element.Elements().ElementAt(8).Value));

                QuizItem item = new QuizItem(question, answerA, answerB, answerC, answerD);
                quizItems.Add(item);
            }

            return quizItems;
        }

        public void CorrectAnswer()
        {
            DrawQuestion();
        }

        public void Timeout()
        {
            DrawQuestion();
        }

        private void DrawQuestion()
        {
            Random random = new Random();
            int number = random.Next(quizItems.Count);
            if (quizItems.Count == 0) return;
            QuizItem oldQuizItem = CurrentQuestion;

            CurrentQuestion = quizItems.ElementAt(number);
         //   quizItems.Remove(CurrentQuestion);

            if (oldQuizItem != null)
            {
            //    quizItems.Add(oldQuizItem);
            }
        }

        #region Initialization

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void InitializeModel()
        {
            base.InitializeModel();
        }

        public override void RegisterCommands()
        {
            base.RegisterCommands();

            Confirm = new DelegateCommand(ConfirmDelegate);
            CloseWindow = new DelegateCommand(CloseWindowDelegate);
        }

        #endregion

        #region Commands handlers

        public ICommand Confirm { get; private set; }
        private void ConfirmDelegate()
        {

        }

        public ICommand CloseWindow { get; private set; }
        private void CloseWindowDelegate()
        {

        }

        #endregion
    }
}
