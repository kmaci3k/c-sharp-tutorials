﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.Regions;
using QuizModule.ViewModels;
using QuizModule.Views;
using Microsoft.Practices.Prism.Modularity;

namespace QuizModule
{
    public class Quiz : IModule
    {
        [Dependency]
        public IUnityContainer Container { get; set; }

        [Dependency]
        public IRegionManager RegionManager { get; set; }

        public void Initialize()
        {
            RegisterTypes();

            QuizViewModel vm = Container.Resolve<QuizViewModel>();
            QuizView view = Container.Resolve<QuizView>(new ParameterOverride("viewModel", vm));

            IRegionManager mainScopeRegionManager = RegionManager.Regions["MainRegion"].Add(view, "QuizView", true);
            
          
            vm.MainScopeRegionManager = mainScopeRegionManager;
            vm.Initialize();
        }

        private void RegisterTypes()
        {
            Container.RegisterType<QuizView>();
            Container.RegisterType<QuizViewModel>();
        }
    }
}
