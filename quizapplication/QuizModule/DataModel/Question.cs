﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuizModule.DataModel
{
    public class Question
    {
        public String Text { get; private set; }

        public Question(String text)
        {
            this.Text = text;
        }
    }
}
