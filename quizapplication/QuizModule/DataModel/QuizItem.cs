﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuizModule.DataModel
{
    public class QuizItem
    {
        public Question Question { get; private set; }
        public Answer AnswerA { get; private set; }
        public Answer AnswerB { get; private set; }
        public Answer AnswerC { get; private set; }
        public Answer AnswerD { get; private set; }

        public QuizItem(Question question, Answer answerA, Answer answerB, Answer answerC, Answer answerD)
        {
            this.Question = question;
            this.AnswerA = answerA;
            this.AnswerB = answerB;
            this.AnswerC = answerC;
            this.AnswerD = answerD;
        }
    }
}
