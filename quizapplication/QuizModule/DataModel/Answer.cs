﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuizModule.DataModel
{
    public class Answer
    {
        public String Text { get; private set; }
        public Boolean IsCorrect { get; private set; }

        public Answer(String text)
            : this(text, false)
        {
            
        }

        public Answer(String text, Boolean isCorrect)
        {
            this.Text = text;
            this.IsCorrect = isCorrect;
        }
    }
}
