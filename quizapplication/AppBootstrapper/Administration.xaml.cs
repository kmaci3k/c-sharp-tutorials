﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using QuizModule.DataModel;
using System.Xml.Linq;

namespace AppBootstrapper
{
    /// <summary>
    /// Interaction logic for Administration.xaml
    /// </summary>
    public partial class Administration : Window
    {

        String path;

        public Administration()
        {
            InitializeComponent();

            ListQuestion.ItemsSource = QuizModule.ViewModels.QuizViewModel.quizItems;
            //   MessageBox.Show(((ListQuestion.ItemsSource as List<QuizItem>).ElementAt(0) as QuizItem).Question.Text);
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void SaveXML_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "xml files (*.xml)|*.xml";


            if (saveFileDialog1.ShowDialog() == true)
            {

                path = saveFileDialog1.FileName;
                // pictureBox1.Image.Save(saveFileDialog1.FileName);

            }
            else
                return;
            //List<QuizItem> data;
            List<QuizItem> data = QuizModule.ViewModels.QuizViewModel.quizItems;

            XDocument doc = new XDocument();
            XElement root = new XElement("quizitems");
            doc.Add(root);

            foreach (QuizItem item in data)
            {
                XElement element = new XElement("quizitem");
                XElement question = new XElement("question", item.Question.Text);
                XElement answerA = new XElement("answerA", item.AnswerA.Text);
                XElement answerB = new XElement("answerB", item.AnswerB.Text);
                XElement answerC = new XElement("answerC", item.AnswerC.Text);
                XElement answerD = new XElement("answerD", item.AnswerD.Text);


                element.Add(question);
                element.Add(answerA);
                element.Add(new XElement("correct", item.AnswerA.IsCorrect));
                element.Add(answerB);
                element.Add(new XElement("correct", item.AnswerB.IsCorrect));
                element.Add(answerC);
                element.Add(new XElement("correct", item.AnswerC.IsCorrect));
                element.Add(answerD);
                element.Add(new XElement("correct", item.AnswerD.IsCorrect));

                root.Add(element);
            }

            doc.Save(path);
            /*
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "xml files (*.xml)|*.xml";


            if (saveFileDialog1.ShowDialog() == true)
            {


                // pictureBox1.Image.Save(saveFileDialog1.FileName);

            }
    */
        }

        public List<QuizItem> loadFromXml(String path)
        {
            List<QuizItem> quizItems = new List<QuizItem>();

            XDocument doc = XDocument.Load(path);
            XElement root = doc.Elements().First();
            foreach (XElement element in root.Elements())
            {
                XElement q = element.Elements().ElementAt(0);
                Question question = new Question(q.Value);

                XElement a = element.Elements().ElementAt(1);
                Answer answerA = new Answer(a.Value, Boolean.Parse(element.Elements().ElementAt(2).Value));

                XElement b = element.Elements().ElementAt(3);
                Answer answerB = new Answer(b.Value, Boolean.Parse(element.Elements().ElementAt(4).Value));

                XElement c = element.Elements().ElementAt(5);
                Answer answerC = new Answer(c.Value, Boolean.Parse(element.Elements().ElementAt(6).Value));

                XElement d = element.Elements().ElementAt(7);
                Answer answerD = new Answer(d.Value, Boolean.Parse(element.Elements().ElementAt(8).Value));

                QuizItem item = new QuizItem(question, answerA, answerB, answerC, answerD);
                quizItems.Add(item);
            }

            return quizItems;
        }

        private void OpenXML_Click(object sender, RoutedEventArgs e)
        {

            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.Filter = "xml files (*.xml)|*.xml";


            if (openFileDialog.ShowDialog() == true)
            {

                ListQuestion.ItemsSource =
                    QuizModule.ViewModels.QuizViewModel.quizItems = loadFromXml(openFileDialog.FileName);
               // MessageBox.Show(QuizModule.ViewModels.QuizViewModel.quizItems.Count.ToString());
                // pictureBox1.Image.Save(saveFileDialog1.FileName);

            }
         
        }
        /*Nowazagadka*/
        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            AdminDetailsWindow win3 = new AdminDetailsWindow(null, ListQuestion);
            var result = win3.ShowDialog();

        }

        private void Agnieszka(object sender, MouseButtonEventArgs e)
        {
            var x = sender as ListBoxItem;
            QuizItem ss=x.DataContext as QuizItem;
            AdminDetailsWindow win3 = new AdminDetailsWindow(ss, ListQuestion);
            var result = win3.ShowDialog();
        }
    }
}

//  return quizItems;
           