﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using QuizModule.DataModel;
using QuizModule.Models;
using QuizModule.Views;

namespace AppBootstrapper
{
    /// <summary>
    /// Interaction logic for AdminDetailsWindow.xaml
    /// </summary>
    public partial class AdminDetailsWindow : Window
    {
        private QuizItem sItem;
        private ListBox sListBox;
        public AdminDetailsWindow(QuizItem sItem, ListBox ListQuestion)
        {

            InitializeComponent();
            this.sItem = sItem;
            this.sListBox = ListQuestion;
            if (sItem != null)
            {
                textBox.Text = sItem.Question.Text;
                textBox_Copy.Text = sItem.AnswerA.Text;
                textBox_Copy1.Text = sItem.AnswerB.Text;
                textBox_Copy2.Text = sItem.AnswerC.Text;
                textBox_Copy3.Text = sItem.AnswerD.Text;
                if (sItem.AnswerA.IsCorrect)
                    slider.Value = 0;
                if (sItem.AnswerB.IsCorrect)
                    slider.Value = 1;
                if (sItem.AnswerC.IsCorrect)
                    slider.Value = 2;
                if (sItem.AnswerD.IsCorrect)
                    slider.Value = 3;
                QuizModule.ViewModels.QuizViewModel.quizItems.Remove(sItem);
            }
            
                
        }

        private void Zapisz_Click(object sender, RoutedEventArgs e)
        {

            Answer a = new Answer(textBox_Copy.Text,slider.Value==0?true:false);
            Answer b = new Answer(textBox_Copy1.Text, slider.Value == 1 ? true : false);
            Answer c = new Answer(textBox_Copy2.Text, slider.Value == 2 ? true : false);
            Answer d = new Answer(textBox_Copy3.Text, slider.Value == 3 ? true : false);

                sItem = new QuizItem(new Question(textBox.Text), a,b,c,d);
          
                QuizModule.ViewModels.QuizViewModel.quizItems.Add(sItem);
                sListBox.ItemsSource =
                    QuizModule.ViewModels.QuizViewModel.quizItems;
                sListBox.Items.Refresh();

            if (QuizModule.ViewModels.QuizViewModel.quizItems.Count == 0)
                QuizView.AgGrid.Visibility = Visibility.Visible;
            else
            {
                QuizView.AgGrid.Visibility = Visibility.Hidden;
            }
           
            this.Close();
        }

        private void Agnieszkajuzdawnotutajniemieszka(object sender, RoutedEventArgs e)
        {

          
            sListBox.ItemsSource =
                QuizModule.ViewModels.QuizViewModel.quizItems;
            sListBox.Items.Refresh();

            if (QuizModule.ViewModels.QuizViewModel.quizItems.Count == 0)
                QuizView.AgGrid.Visibility = Visibility.Visible;
            else
            {
                QuizView.AgGrid.Visibility = Visibility.Hidden;
            }
            this.Close();

        }
      
    }
  public class Validator : ValidationRule
        {
            public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
            {

                var x = value as String;
                int  bb;
                bool b;
                b=int.TryParse(x,out bb);
                if(!b)
                return new ValidationResult(false, "Wrong");
                else
                return new ValidationResult(true, null);
            }
        }
}
