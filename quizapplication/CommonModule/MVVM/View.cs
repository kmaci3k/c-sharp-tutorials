﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonModule.MVVM
{
    public interface View<T> where T : ViewModel
    {
        T ViewModel { get; }
    }
}
