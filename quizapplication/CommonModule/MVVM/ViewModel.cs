﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.Regions;

namespace CommonModule.MVVM
{
    public class ViewModel : ObservableItem
    {
        [Dependency]
        public IUnityContainer Container { get; set; }

        [Dependency]
        public IRegionManager RegionManager { get; set; }

        public IRegionManager MainScopeRegionManager { get; set; }
        public IRegionManager ScopeRegionManager { get; set; }

        public ViewModel()
        { 
            
        }

        public virtual void Initialize()
        {
            RegisterCommands();
            InitializeModel();
        }

        public virtual void InitializeModel()
        { 
            
        }

        public virtual void RegisterCommands()
        { 
        
        }
    }
}
