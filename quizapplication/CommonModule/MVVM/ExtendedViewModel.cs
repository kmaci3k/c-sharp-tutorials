﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonModule.MVVM
{
    public class ExtendedViewModel<T> : ViewModel where T : Model
    {
        private T _model;
        public T Model
        {
            get { return _model; }
            set
            {
                if (_model == null || !_model.Equals(value))
                {
                    _model = value;
                    NotifyPropertyChanged("Model");
                }
            }
        }

        public ExtendedViewModel()
        {

        }

        public ExtendedViewModel(T model)
        {
            Model = model;
        }

        public override void InitializeModel()
        {
            base.InitializeModel();
            if(Model != null)
            {
                Model.Initialize();
            }
        }
    }
}
